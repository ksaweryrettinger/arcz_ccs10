#ifndef INCLUDES_PARAMETERS_ORIGINAL_H_
#define INCLUDES_PARAMETERS_ORIGINAL_H_

#include <limits.h>
#include "tm4c123gh6pm.h"

/* --------------------------- Application  -----------------------------------*/

typedef enum {A, B} eTunerRxMode;

#define DEBUG_DISPLAY      (0)
#define NUM_EXPANDERS      (2)
#define MAX_BYTES_UART_RX  (7)
#define NUM_BUTTON_CHECKS  (5)

/* -------------------------- Debug Switches  ---------------------------------*/

#define DEBUG_MODBUS       (0) //Modbus USB UART
#define DEBUG_RS485        (0) //RS485 USB UART

/* ---------------------------- Expanders  ------------------------------------*/

#define EXPANDER_DUAL_BUTTONS   (0)  //Expander U1
#define EXPANDER_OTHER_BUTTONS  (1)  //Expander U4

/* -------------------------- Tuner Buttons -----------------------------------*/

#define BUTTON_AR    (0x01) //automatyczna (0) / ręczna (1)
#define BUTTON_ZS    (0x02) //zerowanie (0) / strojenie (1)
#define BUTTON_PZ    (0x04) //precyzyjny (0) / zgrubny (1)
#define BUTTON_PM_1  (0x08) //bit 0 of -/0/+ button
#define BUTTON_PM_2  (0x10) //bit 1 of -/0/+ button
#define BUTTON_OFF   (0x20) //offet button (- = 0, + = 1)
#define BUTTON_MSG   (0xC0) //dummy bits (alays ON)

/* --------------------- Futaba Display Instructions --------------------------*/

#define FUTABA_WRITE    (0xFA00) //WRITE TO DDRAM/CGRAM
#define FUTABA_CGRAM    (0xF840) //SET CGRAM ADDRESS 0x00
#define FUTABA_DISPLAY  (0xF80C) //DISPLAY ON
#define FUTABA_NEXTROW  (0xF8C0) //CURSOR AT SECOND ROW
#define FUTABA_HOME     (0xF802) //CURSOR HOME
#define FUTABA_LUMI     (0xF838) //LUMINANCE

/* --------------------- Futaba Display Characters ----------------------------*/

//Standard Character Table
#define CHAR_SPACE  (0x20)
#define CHAR_ERROR  (0x2D)
#define CHAR_MINUS  (0x2D)
#define CHAR_PLUS   (0x2B)
#define CHAR_LEFT   (0x3C)
#define CHAR_RIGHT  (0x3E)
#define CHAR_PERC   (0x25)
#define CHAR_DOT    (0x2E)
#define CHAR_ZERO   (0x30)
#define CHAR_P      (0x50)
#define CHAR_Z      (0x5A)
#define CHAR_A      (0x41)
#define CHAR_R      (0x52)
#define CHAR_S      (0x53)
#define CHAR_K      (0x4B)
#define CHAR_N      (0x4E)

//Custom Characters
#define CHAR_LFAST  (0x00)
#define CHAR_RFAST  (0x01)
#define CHAR_PMINUS (0x02)
#define CHAR_RINV   (0x03)
#define CHAR_FINV   (0x04)

/* ------------------------------- Timers -------------------------------------*/

#define TIMEOUT_RS485   (5000000)  //100ms
#define TIMEOUT_FUTABA  (50000000) //1000ms
#define DELAY_REQUEST   (12500000) //250ms

/* ------------------------- SSI Configuration --------------------------------*/

#define SSI_BITRATE    (1000000) //1MHz
#define SSI_FRAMESIZE  (16)

/* ------------------------- I2C Configuration  -------------------------------*/

#define I2C_SPEED           (100000) //100kHz
#define I2C_MASTER_TIMEOUT  (15000)  //equivalent to 30 I2C clock cycles
#define I2C_MSG_NUMBYTES    (2)

/* ----------------------- Modbus Configuration -------------------------------*/

#define MB_MODE                 (MB_RTU)
#define MB_SLAVEID              (0x01)
#define MB_PORT                 (0)
#define MB_BAUDRATE             (19200)
#define MB_PARITY               (MB_PAR_EVEN)

/* --------------------------- Modbus LED -------------------------------------*/

#define MB_LED_PERIPHERAL       (SYSCTL_PERIPH_GPIOB)
#define MB_LED_PORT             (GPIO_PORTB_BASE)
#define MB_LED_PIN              (GPIO_PIN_5)

#if (DEBUG_RS485)

   #define RS485_UART_MODULE       (UART0_BASE)
   #define RS485_UART_INT          (INT_UART0)
   #define RS485_UART_PERIPH       (SYSCTL_PERIPH_UART0)
   #define RS485_UART_GPIO_PORT    (GPIO_PORTA_BASE)
   #define RS485_UART_GPIO_PIN_RX  (GPIO_PIN_0)
   #define RS485_UART_GPIO_PIN_TX  (GPIO_PIN_1)
   #define RS485_UART_GPIO_RX      (GPIO_PA0_U0RX)
   #define RS485_UART_GPIO_TX      (GPIO_PA1_U0TX)

#else

   #define RS485_UART_MODULE       (UART4_BASE)
   #define RS485_UART_INT          (INT_UART4)
   #define RS485_UART_PERIPH       (SYSCTL_PERIPH_UART4)
   #define RS485_UART_GPIO_PORT    (GPIO_PORTC_BASE)
   #define RS485_UART_GPIO_PIN_RX  (GPIO_PIN_4)
   #define RS485_UART_GPIO_PIN_TX  (GPIO_PIN_5)
   #define RS485_UART_GPIO_RX      (GPIO_PC4_U4RX)
   #define RS485_UART_GPIO_TX      (GPIO_PC5_U4TX)

#endif

/* --------------------------- Modbus UART ------------------------------------*/

#if (DEBUG_MODBUS)

   #define MB_UART_MODULE       (UART0_BASE)
   #define MB_UART_INT          (INT_UART0)
   #define MB_UART_PERIPH       (SYSCTL_PERIPH_UART0)
   #define MB_UART_GPIO_PORT    (GPIO_PORTA_BASE)
   #define MB_UART_GPIO_PIN_RX  (GPIO_PIN_0)
   #define MB_UART_GPIO_PIN_TX  (GPIO_PIN_1)
   #define MB_UART_GPIO_RX      (GPIO_PA0_U0RX)
   #define MB_UART_GPIO_TX      (GPIO_PA1_U0TX)

#else

   #define MB_UART_MODULE       (UART3_BASE)
   #define MB_UART_INT          (INT_UART3)
   #define MB_UART_PERIPH       (SYSCTL_PERIPH_UART3)
   #define MB_UART_GPIO_PORT    (GPIO_PORTC_BASE)
   #define MB_UART_GPIO_PIN_RX  (GPIO_PIN_6)
   #define MB_UART_GPIO_PIN_TX  (GPIO_PIN_7)
   #define MB_UART_GPIO_RX      (GPIO_PC6_U3RX)
   #define MB_UART_GPIO_TX      (GPIO_PC7_U3TX)

#endif

/* ------------------------- Modbus Registers ---------------------------------*/

#define COILS_START               (1)      //coils starting address
#define COILS_NUM                 (0)      //number of coils

#define DISCRETES_START           (10001)  //discretes starting addres
#define DISCRETES_NUM             (0)      //number of discrete inputs

#define INPUT_REGISTERS_START     (30001)  //input registers starting address
#define INPUT_REGISTERS_NUM       (16)     //number of input registers

#define HOLDING_REGISTERS_START   (40001)  //holding registers starting address
#define HOLDING_REGISTERS_NUM     (0)      //number of holding registers

#define REG_NUM_BYTES             (2)      //number of bytes in register
#define REG_ADDR_OFFSET           (1)      //bit/register address offset

/* ------------------- Modbus Register Configuration --------------------------*/

//INPUT REGISTERS - BUTTONS AND TUNER DATA
#define INPUTREG_EXPANDER_U1   (1)
#define INPUTREG_EXPANDER_U4   (2)
#define INPUTREG_TUNERA_START  (3)
#define INPUTREG_TUNERB_START  (9)

//INPUT REGISTERS - ERRORS
#define INPUTREG_I2C_ERRORS     (15)
#define INPUTREG_RS485_TIMEOUT  (16)

//I2C ERROR MASKS
#define MASK_I2C_ERROR_U1      (0x10)
#define MASK_I2C_ERROR_U4      (0x20)

/* --------------------- Modbus Callback Functions ----------------------------*/

#define MBCB_COILS_DEFAULT      (1)
#define MBCB_DISCRETE_DEFAULT   (1)
#define MBCB_INPUTREG_DEFAULT   (1)
#define MBCB_HOLDING_DEFAULT    (1)

/* ------------------------- Modbus Global Registers --------------------------*/

uint8_t  ucMBCoils[(COILS_NUM + (CHAR_BIT - 1)) / CHAR_BIT];            //Coils
uint8_t  ucMBDiscretes[(DISCRETES_NUM + (CHAR_BIT - 1)) / CHAR_BIT];    //Discrete Inputs
uint16_t usMBInputReg[INPUT_REGISTERS_NUM];                             //Input Registers
uint16_t usMBHoldingReg[HOLDING_REGISTERS_NUM];                         //Holding Registers

/* ------------------------- Macro Used to Hide Warnings ----------------------*/

#define UNREFERENCED(x)  ((void)x)

#endif //INCLUDES_PARAMETERS_H_
