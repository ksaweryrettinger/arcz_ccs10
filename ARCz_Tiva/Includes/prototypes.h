#ifndef INCLUDES_PROTOTYPES_H_
#define INCLUDES_PROTOTYPES_H_

#include "parameters.h"

/* ----------------------- I2C Function Prototypes ------------------------------------*/

bool bReceiveI2C(uint32_t, uint8_t*, uint8_t, uint8_t);
bool bWriteI2C(uint32_t, uint8_t*, uint8_t, uint8_t);
void xI2CMasterWait(uint32_t, uint32_t*);
void FutabaWrite(uint16_t);
void StartTimerRS485(void);
void StopTimerRS485(void);
void StartTimerFutaba(void);
void StopTimerFutaba(void);

#endif
