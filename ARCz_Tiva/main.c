// Automatyczna Regulacja Częstotliwości (ARCz) - TM4C123GH6PM
// Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/*---------------------------------- Includes ---------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include "prototypes.h"
#include "parameters.h"

/*------------------------------ Static Variables -----------------------------------------*/

static volatile uint8_t ucBufferUART[MAX_BYTES_UART_RX] = { 0 };
static volatile uint8_t ucByteNumUART = 0;
static volatile bool bNewButtonsA = false;
static volatile bool bNewButtonsB = false;
static volatile bool bTunerATx = false; //tuner A message transmitted flag
static volatile bool bTunerBTx = false; //tuner B message transmitted flag
static volatile bool bTunerARx = false; //tuner A message received flag
static volatile bool bTunerBRx = false; //tuner B message received flag
static volatile bool bTimeoutRS485 = false;
static volatile bool bTimeoutTunerA = false;
static volatile bool bTimeoutTunerB = false;
static volatile bool bReconnectedTunerA = false;
static volatile bool bReconnectedTunerB = false;
static volatile bool bFillDisplay = false;
static volatile bool bReadNextTuner = true;
static volatile eTunerRxMode eRxMode = A;
static uint8_t ucButtonsTunerA = 0;
static uint8_t ucButtonsTunerB = 0;

int main(void)
{
    /*-------------------------- FPU Module -----------------------------------------------*/

    FPULazyStackingEnable();
    FPUEnable();

    /*------------------------ Expander Addressing ----------------------------------------*/

    uint8_t ucAddressExpander[NUM_EXPANDERS] = { 0b0100000,   //Expander U1 (Dual-State Buttons)
                                                 0b0100001 }; //Expander U4 (Triple-State Buttons)

    /*------------------------- Other Variables -------------------------------------------*/

    uint8_t ucExpanderU1 = 0;
    uint8_t ucExpanderU4 = 0;
    uint8_t ucTempButtonsTunerA = 0;
    uint8_t ucTempButtonsTunerB = 0;
    uint8_t ucButtonCountTunerA = 0;
    uint8_t ucButtonCountTunerB = 0;
    uint8_t i;
    uint8_t ucDataTunerA[MAX_BYTES_UART_RX] = { 0 };
    uint8_t ucDataTunerB[MAX_BYTES_UART_RX] = { 0 };
    uint8_t ucFutabaLuminance = 0;
    uint16_t usFutabaError = 0;
    bool bDisplayON = false;
    bool bDataReady = false;
    bool bRefreshDisplay = false;
    bool bFutabaTimerEnabled = false;
    bool bLuminanceChange = false;

    /*-------------------------------- System Clock Configuration (50MHz) ----------------------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*------------------------------------------ GPIO Peripherals ------------------------------------------*/

    //Enable GPIO peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB); //I2C, GPIO (Modbus LED)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC); //UART (Modbus, RS-485)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD); //SSI CLK, MOSI (Futaba Display)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE); //SSI Chip-Select GPIO (Futaba Display)

    /*-------------------------------------- RS-485 UART Configuration -------------------------------------*/

    //RS485 Rx/Tx GPIO
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_4);
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, 0); //Rx mode

    //Pin configuration
    GPIOPinConfigure(RS485_UART_GPIO_RX);
    GPIOPinConfigure(RS485_UART_GPIO_TX);
    GPIOPinTypeUART(RS485_UART_GPIO_PORT, RS485_UART_GPIO_PIN_RX | RS485_UART_GPIO_PIN_TX);

    //Module configuration
    SysCtlPeripheralEnable(RS485_UART_PERIPH);
    UARTConfigSetExpClk(RS485_UART_MODULE, SysCtlClockGet(), 19200,
    (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_EVEN));
    UARTFIFODisable(RS485_UART_MODULE);
    IntEnable(RS485_UART_INT);
    UARTIntEnable(RS485_UART_MODULE, UART_INT_RX | UART_INT_OE);

    /*------------------------------------------ SSI Configuration -----------------------------------------*/

    //Peripheral configuration
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI3);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_SSI3)) {};

    //Pin configuraiton
    GPIOPinConfigure(GPIO_PD0_SSI3CLK); //clock
    GPIOPinConfigure(GPIO_PD3_SSI3TX);  //MOSI
    GPIOPinTypeSSI(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_3);

    //Chip-select pin
    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_3);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, GPIO_PIN_3);

    //Module configuration
    SSIConfigSetExpClk(SSI3_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_3, //CPOL = 1, CPHA = 0
                       SSI_MODE_MASTER, SSI_BITRATE, SSI_FRAMESIZE);     //2Mhz, 16-bits
    SSIEnable(SSI3_BASE);

    SysCtlDelay((SysCtlClockGet()/3/1000000) * 100); //100us delay

    /*------------------------------------------ I2C Configuration -----------------------------------------*/

    //Pin configuration
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
    I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), false); //standard speed

    /*------------------------------------------ I2C Timeout Timer -----------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER1A);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);

    /*----------------------------------------- RS485 Response Timer ---------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
    TimerConfigure(TIMER2_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER2A);
    TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);

    /*------------------------------------ Futaba Display Blinking Timer -----------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
    TimerConfigure(TIMER3_BASE, TIMER_CFG_PERIODIC);
    IntEnable(INT_TIMER3A);
    TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);

    /*--------------------------------------- RS485 Data Request Timer -------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER4);
    TimerConfigure(TIMER4_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER4A);
    TimerIntEnable(TIMER4_BASE, TIMER_TIMA_TIMEOUT);

    /*--------------------------------- MODBUS Protocol Stack Initialization -------------------------------*/

    //Note: Modbus GPIO and UART configured in parameters.h
    (void) eMBInit(MB_MODE, MB_SLAVEID, MB_PORT, MB_BAUDRATE, MB_PARITY);
    (void) eMBEnable();

    /*---------------------------------------- Clear Futaba Display ----------------------------------------*/

    FutabaWrite(0xF801);

    /*----------------------------------- Futaba Display Custom Characters ---------------------------------*/

    //Access CGRAM
    FutabaWrite(FUTABA_CGRAM);

    //Create << character (character address 0x00)
    FutabaWrite(FUTABA_WRITE | 0x02);
    FutabaWrite(FUTABA_WRITE | 0x04);
    FutabaWrite(FUTABA_WRITE | 0x09);
    FutabaWrite(FUTABA_WRITE | 0x12);
    FutabaWrite(FUTABA_WRITE | 0x09);
    FutabaWrite(FUTABA_WRITE | 0x04);
    FutabaWrite(FUTABA_WRITE | 0x02);
    FutabaWrite(FUTABA_WRITE | 0x00);

    //Create >> character (character address 0x01)
    FutabaWrite(FUTABA_WRITE | 0x08);
    FutabaWrite(FUTABA_WRITE | 0x04);
    FutabaWrite(FUTABA_WRITE | 0x12);
    FutabaWrite(FUTABA_WRITE | 0x09);
    FutabaWrite(FUTABA_WRITE | 0x12);
    FutabaWrite(FUTABA_WRITE | 0x04);
    FutabaWrite(FUTABA_WRITE | 0x08);
    FutabaWrite(FUTABA_WRITE | 0x10);

    //Create +- character (character address 0x02)
    FutabaWrite(FUTABA_WRITE | 0x04);
    FutabaWrite(FUTABA_WRITE | 0x04);
    FutabaWrite(FUTABA_WRITE | 0x1F);
    FutabaWrite(FUTABA_WRITE | 0x04);
    FutabaWrite(FUTABA_WRITE | 0x04);
    FutabaWrite(FUTABA_WRITE | 0x00);
    FutabaWrite(FUTABA_WRITE | 0x1F);
    FutabaWrite(FUTABA_WRITE | 0x00);

    //Create inverted R character (character address 0x03)
    FutabaWrite(FUTABA_WRITE | 0x01);
    FutabaWrite(FUTABA_WRITE | 0x0E);
    FutabaWrite(FUTABA_WRITE | 0x0E);
    FutabaWrite(FUTABA_WRITE | 0x01);
    FutabaWrite(FUTABA_WRITE | 0x0B);
    FutabaWrite(FUTABA_WRITE | 0x0D);
    FutabaWrite(FUTABA_WRITE | 0x0E);
    FutabaWrite(FUTABA_WRITE | 0x1F);

    //Create inverted F character (character address 0x04)
    FutabaWrite(FUTABA_WRITE | 0x00);
    FutabaWrite(FUTABA_WRITE | 0x0F);
    FutabaWrite(FUTABA_WRITE | 0x0F);
    FutabaWrite(FUTABA_WRITE | 0x0F);
    FutabaWrite(FUTABA_WRITE | 0x01);
    FutabaWrite(FUTABA_WRITE | 0x0F);
    FutabaWrite(FUTABA_WRITE | 0x0F);
    FutabaWrite(FUTABA_WRITE | 0x0F);

    //Reset cursor to DDRAM start address
    FutabaWrite(FUTABA_HOME);

    while (1)
    {
        /*----------------------------------------------- Modbus -----------------------------------------------*/

        (void) eMBPoll();

        /*---------------------------------------------- Expanders ---------------------------------------------*/

        //Normal buttons
        if (bReceiveI2C(I2C0_BASE, &ucExpanderU1, 1, ucAddressExpander[0]))
        {
            ucExpanderU1 = ~ucExpanderU1;
            usMBInputReg[INPUTREG_EXPANDER_U1 - 1] = (uint16_t) ucExpanderU1;
            usMBInputReg[INPUTREG_I2C_ERRORS - 1] &= ~(MASK_I2C_ERROR_U1);
        }
        else usMBInputReg[INPUTREG_I2C_ERRORS - 1] |= MASK_I2C_ERROR_U1;

        //Triple-state and other buttons
        if (bReceiveI2C(I2C0_BASE, &ucExpanderU4, 1, ucAddressExpander[1]))
        {
            ucExpanderU4 = ~ucExpanderU4;
            usMBInputReg[INPUTREG_EXPANDER_U4 - 1] = (uint16_t) ucExpanderU4;
            usMBInputReg[INPUTREG_I2C_ERRORS - 1] &= ~(MASK_I2C_ERROR_U4);
        }
        else usMBInputReg[INPUTREG_I2C_ERRORS - 1] |= MASK_I2C_ERROR_U4;

        /*-------------------------------------------- Tuner Buttons -------------------------------------------*/

        if (!bTunerBTx && !bTunerARx && !bTunerBRx) //ensure that RS-485 line is not busy
        {
            //Tuner A
            ucTempButtonsTunerA = ucExpanderU1 & 0x07;
            ucTempButtonsTunerA |= (ucExpanderU4 & 0x03) << 3;
            ucTempButtonsTunerA |= (ucExpanderU4 & 0x10) << 1;

            //Transmit new button configuration
            if (ucButtonsTunerA != ucTempButtonsTunerA)
            {
                //Store new button configuration
                ucButtonsTunerA = ucTempButtonsTunerA;

                //Reset safety counter
                ucButtonCountTunerA = 0;
            }
            else if (ucButtonCountTunerA == (NUM_BUTTON_CHECKS - 1) || bReconnectedTunerA) //transmit new button configuration
            {
                //Set RS485 module to Tx mode
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_PIN_4);

                //Transmit new button configuration to Tuner A
                UARTCharPut(RS485_UART_MODULE, 'A');
                UARTCharPut(RS485_UART_MODULE, ucButtonsTunerA | BUTTON_MSG); //append dummy bit (6) for unique ASCII
                while (UARTBusy(RS485_UART_MODULE));

                //Set RS485 module to Rx mode
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, 0);

                //Start RS-485 response timer
                StartTimerRS485();

                //Set flag and update counter
                bTunerATx = true;
                bNewButtonsA = true;
                bReconnectedTunerA = false;
                ucButtonCountTunerA++;
                bRefreshDisplay = true;
            }
            else if (ucButtonCountTunerA < (NUM_BUTTON_CHECKS - 1)) ucButtonCountTunerA++; //increment safety counter
        }

        //Tuner B
        if (!bTunerATx && !bTunerARx && !bTunerBRx) //ensure that RS-485 line is not busy
        {
            ucTempButtonsTunerB = (ucExpanderU1 & 0x38) >> 3;
            ucTempButtonsTunerB |= (ucExpanderU4 & 0x0C) << 1;
            ucTempButtonsTunerB |= (ucExpanderU4 & 0x10) << 1;

            //Transmit new button configuration
            if (ucButtonsTunerB != ucTempButtonsTunerB)
            {
                //Store new button configuration
                ucButtonsTunerB = ucTempButtonsTunerB;

                //Reset safety counter
                ucButtonCountTunerB = 0;
            }
            else if (ucButtonCountTunerB == (NUM_BUTTON_CHECKS - 1) || bReconnectedTunerB) //transmit new button configuration
            {
                //Set RS485 module to Tx mode
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_PIN_4);

                //Transmit new button configuration to Tuner B
                UARTCharPut(RS485_UART_MODULE, 'B');
                UARTCharPut(RS485_UART_MODULE, ucButtonsTunerB | BUTTON_MSG); //append dummy bit for unique ASCII
                while (UARTBusy(RS485_UART_MODULE));

                //Set RS485 module to Rx mode
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, 0);

                //Start RS-485 response timer
                StartTimerRS485();

                //Set flag and update counter
                bTunerBTx = true;
                bNewButtonsB = true;
                bReconnectedTunerB = false;
                ucButtonCountTunerB++;
                bRefreshDisplay = true;
            }
            else if (ucButtonCountTunerB < (NUM_BUTTON_CHECKS - 1)) ucButtonCountTunerB++; //increment safety counter
        }

        /*--------------------------------------------- Tuner Data ---------------------------------------------*/

        if (!bTunerATx && !bTunerBTx && !bTunerARx && !bTunerBRx) //ensure that RS-485 line is not busy
        {
            if (eRxMode == A && bReadNextTuner) //send read command to Tuner A
            {
                //Set RS485 module to Tx mode
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_PIN_4);

                //Send command
                UARTCharPut(RS485_UART_MODULE, 'A');
                UARTCharPut(RS485_UART_MODULE, '?');
                while (UARTBusy(RS485_UART_MODULE));

                //Set RS485 module to Rx mode
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, 0);

                //Start RS-485 response timer
                StartTimerRS485();

                //Clear flag and start timer
                bReadNextTuner = false;
                TimerLoadSet(TIMER4_BASE, TIMER_BOTH, DELAY_REQUEST);
                TimerEnable(TIMER4_BASE, TIMER_BOTH);

                //Update flags
                bTunerATx = true;
            }
            else if (eRxMode == B && bReadNextTuner) //send read command to Tuner B
            {
                //Set RS485 module to Tx mode
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_PIN_4);

                //Send command
                UARTCharPut(RS485_UART_MODULE, 'B');
                UARTCharPut(RS485_UART_MODULE, '?');
                while (UARTBusy(RS485_UART_MODULE));

                //Set RS485 module to Rx mode
                GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, 0);

                //Start RS-485 response timer
                StartTimerRS485();

                //Clear flag and start timer
                bReadNextTuner = false;
                TimerLoadSet(TIMER4_BASE, TIMER_BOTH, DELAY_REQUEST);
                TimerEnable(TIMER4_BASE, TIMER_BOTH);

                //Update flags
                bTunerBTx = true;
            }
        }

        if (bTunerARx || bTunerBRx) //response received
        {
            //Disable interrupts
            IntDisable(RS485_UART_INT);

            //Copy tuner data
            for (i = 1; i < MAX_BYTES_UART_RX; i++)
            {
                if (bTunerARx)
                {
                    if (ucDataTunerA[i] != ucBufferUART[i])
                    {
                        ucDataTunerA[i] = ucBufferUART[i];
                        usMBInputReg[INPUTREG_TUNERA_START + (i - 2)] = (uint16_t) ucBufferUART[i];
                        bRefreshDisplay = true; //new data, signal display refresh
                    }
                }
                else if (bTunerBRx)
                {
                    if (ucDataTunerB[i] != ucBufferUART[i])
                    {
                        ucDataTunerB[i] = ucBufferUART[i];
                        usMBInputReg[INPUTREG_TUNERB_START + (i - 2)] = (uint16_t) ucBufferUART[i];
                        bRefreshDisplay = true; //new data, signal display refresh
                    }
                }
            }

            //Re-enable interrupts
            IntEnable(RS485_UART_INT);

            //Clear Tuner B receive flag
            if (bTunerARx || bTunerBRx)
            {
                if (!bDataReady) bDataReady = true;
                bTunerARx = false;
                bTunerBRx = false;
            }
        }

        /*-------------------------------------------- Timeout Errors ------------------------------------------*/

        if (bTimeoutRS485)
        {
            if (bTunerATx) //Tuner A
            {
                //Update flags
                bTimeoutTunerA = true;
                bTimeoutRS485 = false;
                bTunerATx = false;
            }
            else if (bTunerBTx) //Tuner B
            {
                //Update flags
                bTimeoutTunerB = true;
                bTimeoutRS485 = false;
                bTunerBTx = false;
            }
        }

        //Update error register (Tuner A)
        if (bTimeoutTunerA) usMBInputReg[INPUTREG_RS485_TIMEOUT - 1] |= 0x0001;
        else usMBInputReg[INPUTREG_RS485_TIMEOUT - 1] &= ~(0x0001);

        //Update error register (Tuner B)
        if (bTimeoutTunerB) usMBInputReg[INPUTREG_RS485_TIMEOUT - 1] |= 0x0002;
        else usMBInputReg[INPUTREG_RS485_TIMEOUT - 1] &= ~(0x0002);

        /*---------------------------------------- Futaba Luminance Button -------------------------------------*/

        if ((ucExpanderU4 & 0x10) && !bLuminanceChange) //button pressed
        {
            bLuminanceChange = true;
        }
        else if (!(ucExpanderU4 & 0x10) && bLuminanceChange)  //button released
        {
            bLuminanceChange = false;

            if (!ucFutabaLuminance) ucFutabaLuminance = 0x03; //reset luminance to 25%
            else ucFutabaLuminance--; //increase luminance by 25%

            //Send Futaba command
            FutabaWrite(FUTABA_LUMI | ucFutabaLuminance);
        }

        /*-------------------------------------------- Futaba Display ------------------------------------------*/

        #if (!DEBUG_DISPLAY)

        //Timeout errors on both tuners - signal data ready
        if (bTimeoutTunerA && bTimeoutTunerB && !bDataReady)
        {
            bDataReady = true;
            bRefreshDisplay = true;
        }

        //Enable/disable Futaba blinking character timer
        if (bTimeoutTunerA || bTimeoutTunerB) //ENABLE
        {
            bRefreshDisplay = true;

            if (!bFutabaTimerEnabled)
            {
               StartTimerFutaba();
               bFutabaTimerEnabled = true;
            }

            if (bFillDisplay) usFutabaError = CHAR_ERROR;
            else usFutabaError = CHAR_SPACE;
        }
        else if (bFutabaTimerEnabled) //DISABLE
        {
            bFutabaTimerEnabled = false;
            StopTimerFutaba();
        }

        //Refresh Futaba Display
        if (bDataReady && bRefreshDisplay)
        {
            bRefreshDisplay = false; //refresh only once

            /*---------------------------------------------- First Row ---------------------------------------------*/

            //Precyzyjny/Zgrubny
            if (!(ucButtonsTunerA & BUTTON_AR)) FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //empty space
            else if (!(ucButtonsTunerA & BUTTON_PZ)) FutabaWrite(FUTABA_WRITE | CHAR_P); //precyzyjny
            else if (ucButtonsTunerA & BUTTON_PZ) FutabaWrite(FUTABA_WRITE | CHAR_Z); //zgrubny

            //Automatyczna/Ręczna
            if (!(ucButtonsTunerA & BUTTON_AR)) FutabaWrite(FUTABA_WRITE | CHAR_A); //automatyczna
            else if (ucButtonsTunerA & BUTTON_AR) FutabaWrite(FUTABA_WRITE | CHAR_R); //ręczna

            //Tuner A Large Data
            if (bTimeoutTunerA)
            {
                //Display blinking errors
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
            }
            else
            {
               //R/F Errors
               if (ucDataTunerA[6] & 0x10) FutabaWrite(FUTABA_WRITE | CHAR_RINV);
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE);

               //Left Movement Symbols
               if (ucDataTunerA[5] & 0x01) FutabaWrite(FUTABA_WRITE | CHAR_LEFT); //moving left
               else if (ucDataTunerA[5] & 0x02) FutabaWrite(FUTABA_WRITE | CHAR_LFAST); //moving fast left
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //not moving

               //Data OR Left/Right Extremes
               if (!(ucDataTunerA[6] & 0x01) && !(ucDataTunerA[6] & 0x02)) //display Reading
               {
                   FutabaWrite(FUTABA_WRITE | (CHAR_ZERO + 1));
                   FutabaWrite(FUTABA_WRITE | ucDataTunerA[1]);
                   FutabaWrite(FUTABA_WRITE | CHAR_DOT);
                   FutabaWrite(FUTABA_WRITE | ucDataTunerA[2]);
               }
               else if (ucDataTunerA[6] & 0x01) //left extreme
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_K);
                   FutabaWrite(FUTABA_WRITE | CHAR_R);
                   FutabaWrite(FUTABA_WRITE | CHAR_N);
                   FutabaWrite(FUTABA_WRITE | CHAR_MINUS);
               }
               else if (ucDataTunerA[6] & 0x02) //right extreme
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_K);
                   FutabaWrite(FUTABA_WRITE | CHAR_R);
                   FutabaWrite(FUTABA_WRITE | CHAR_N);
                   FutabaWrite(FUTABA_WRITE | CHAR_PLUS);
               }

               //Right Movement Symbols
               if (ucDataTunerA[5] & 0x04) FutabaWrite(FUTABA_WRITE | CHAR_RIGHT); //moving right
               else if (ucDataTunerA[5] & 0x08) FutabaWrite(FUTABA_WRITE | CHAR_RFAST); //moving fast right
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //not moving

               //Empty Space
               FutabaWrite(FUTABA_WRITE | CHAR_SPACE);
            }

            //Tuner B Large Data
            if (bTimeoutTunerB)
            {
               //Display blinking errors
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
            }
            else
            {
               //Empty Space
               FutabaWrite(FUTABA_WRITE | CHAR_SPACE);

               //Left Movement Symbols
               if (ucDataTunerB[5] & 0x01) FutabaWrite(FUTABA_WRITE | CHAR_LEFT); //moving left
               else if (ucDataTunerB[5] & 0x02) FutabaWrite(FUTABA_WRITE | CHAR_LFAST); //moving fast left
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //not moving

               //Data OR Left/Right Extremes
               if (!(ucDataTunerB[6] & 0x01) && !(ucDataTunerB[6] & 0x02)) //display Reading
               {
                   FutabaWrite(FUTABA_WRITE | (CHAR_ZERO + 1));
                   FutabaWrite(FUTABA_WRITE | ucDataTunerB[1]);
                   FutabaWrite(FUTABA_WRITE | CHAR_DOT);
                   FutabaWrite(FUTABA_WRITE | ucDataTunerB[2]);
               }
               else if (ucDataTunerB[6] & 0x01) //left extreme
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_K);
                   FutabaWrite(FUTABA_WRITE | CHAR_R);
                   FutabaWrite(FUTABA_WRITE | CHAR_N);
                   FutabaWrite(FUTABA_WRITE | CHAR_MINUS);
               }
               else if (ucDataTunerB[6] & 0x02) //right extreme
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_K);
                   FutabaWrite(FUTABA_WRITE | CHAR_R);
                   FutabaWrite(FUTABA_WRITE | CHAR_N);
                   FutabaWrite(FUTABA_WRITE | CHAR_PLUS);
               }

               //Right Movement Symbols
               if (ucDataTunerB[5] & 0x04) FutabaWrite(FUTABA_WRITE | CHAR_RIGHT); //moving right
               else if (ucDataTunerB[5] & 0x08) FutabaWrite(FUTABA_WRITE | CHAR_RFAST); //moving fast right
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //not moving

               //R/F Errors
               if (ucDataTunerB[6] & 0x10) FutabaWrite(FUTABA_WRITE | CHAR_RINV);
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE);
            }

            //Automatyczna/Ręczna
            if (!(ucButtonsTunerB & BUTTON_AR)) FutabaWrite(FUTABA_WRITE | CHAR_A); //automatyczna
            else if (ucButtonsTunerB & BUTTON_AR) FutabaWrite(FUTABA_WRITE | CHAR_R); //ręczna

            //Precyzyjny/Zgrubny
            if (!(ucButtonsTunerB & BUTTON_AR)) FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //empty space
            else if (!(ucButtonsTunerB & BUTTON_PZ)) FutabaWrite(FUTABA_WRITE | CHAR_P); //precyzyjny
            else if (ucButtonsTunerB & BUTTON_PZ) FutabaWrite(FUTABA_WRITE | CHAR_Z); //zgrubny

            /*---------------------------------------------- Second Row --------------------------------------------*/

            //Set cursor at start of second row (0xC0)
            FutabaWrite(FUTABA_NEXTROW);

            //Plus (+)/Zero (0)/Minus (-)
            if (!(ucButtonsTunerA & (BUTTON_PM_1 | BUTTON_PM_2))) FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //Zero (0)
            else if (ucButtonsTunerA & BUTTON_PM_1) FutabaWrite(FUTABA_WRITE | CHAR_MINUS); //Minus (-)
            else if (ucButtonsTunerA & BUTTON_PM_2) FutabaWrite(FUTABA_WRITE | CHAR_PLUS); //Plus (+)

            //Zerowanie/Strojenie
            if (!(ucButtonsTunerA & BUTTON_ZS)) FutabaWrite(FUTABA_WRITE | CHAR_ZERO); //Zerowanie
            else if (ucButtonsTunerA & BUTTON_ZS) FutabaWrite(FUTABA_WRITE | CHAR_S); //Strojenie

            //Tuner A Small Data
            if (bTimeoutTunerA)
            {
                //Display blinking errors
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
                FutabaWrite(FUTABA_WRITE | usFutabaError);
            }
            else
            {
               //R/F Errors
               if (ucDataTunerA[6] & 0x10) FutabaWrite(FUTABA_WRITE | CHAR_FINV);
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE);

               //Left Movement Symbols
               if (ucDataTunerA[5] & 0x10) FutabaWrite(FUTABA_WRITE | CHAR_LEFT); //moving left
               else if (ucDataTunerA[5] & 0x20) FutabaWrite(FUTABA_WRITE | CHAR_LFAST); //moving fast left
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //not moving

               //Data OR Left/Right Extremes
               if (!(ucDataTunerA[6] & 0x04) && !(ucDataTunerA[6] & 0x08)) //display Reading
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_SPACE);
                   FutabaWrite(FUTABA_WRITE | ucDataTunerA[3]);
                   FutabaWrite(FUTABA_WRITE | ucDataTunerA[4]);
                   FutabaWrite(FUTABA_WRITE | CHAR_PERC);
               }
               else if (ucDataTunerA[6] & 0x04) //left extreme
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_K);
                   FutabaWrite(FUTABA_WRITE | CHAR_R);
                   FutabaWrite(FUTABA_WRITE | CHAR_N);
                   FutabaWrite(FUTABA_WRITE | CHAR_MINUS);
               }
               else if (ucDataTunerA[6] & 0x08) //right extreme
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_K);
                   FutabaWrite(FUTABA_WRITE | CHAR_R);
                   FutabaWrite(FUTABA_WRITE | CHAR_N);
                   FutabaWrite(FUTABA_WRITE | CHAR_PLUS);
               }

               //Right Movement Symbols
               if (ucDataTunerA[5] & 0x40) FutabaWrite(FUTABA_WRITE | CHAR_RIGHT); //moving right
               else if (ucDataTunerA[5] & 0x80) FutabaWrite(FUTABA_WRITE | CHAR_RFAST); //moving fast right
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //not moving

               //Empty Space
               FutabaWrite(FUTABA_WRITE | CHAR_SPACE);
            }

            //Tuner B Small Data
            if (bTimeoutTunerB)
            {
               //Display blinking errors
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
               FutabaWrite(FUTABA_WRITE | usFutabaError);
            }
            else
            {
               //Empty Space
               FutabaWrite(FUTABA_WRITE | CHAR_SPACE);

               //Left Movement Symbols
               if (ucDataTunerB[5] & 0x10) FutabaWrite(FUTABA_WRITE | CHAR_LEFT); //moving left
               else if (ucDataTunerB[5] & 0x20) FutabaWrite(FUTABA_WRITE | CHAR_LFAST); //moving fast left
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //not moving

               //Data OR Left/Right Extremes
               if (!(ucDataTunerB[6] & 0x04) && !(ucDataTunerB[6] & 0x08)) //display Reading
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_SPACE);
                   FutabaWrite(FUTABA_WRITE | ucDataTunerB[3]);
                   FutabaWrite(FUTABA_WRITE | ucDataTunerB[4]);
                   FutabaWrite(FUTABA_WRITE | CHAR_PERC);
               }
               else if (ucDataTunerB[6] & 0x04) //left extreme
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_K);
                   FutabaWrite(FUTABA_WRITE | CHAR_R);
                   FutabaWrite(FUTABA_WRITE | CHAR_N);
                   FutabaWrite(FUTABA_WRITE | CHAR_MINUS);
               }
               else if (ucDataTunerB[6] & 0x08) //right extreme
               {
                   FutabaWrite(FUTABA_WRITE | CHAR_K);
                   FutabaWrite(FUTABA_WRITE | CHAR_R);
                   FutabaWrite(FUTABA_WRITE | CHAR_N);
                   FutabaWrite(FUTABA_WRITE | CHAR_PLUS);
               }

               //Right Movement Symbols
               if (ucDataTunerB[5] & 0x40) FutabaWrite(FUTABA_WRITE | CHAR_RIGHT); //moving right
               else if (ucDataTunerB[5] & 0x80) FutabaWrite(FUTABA_WRITE | CHAR_RFAST); //moving fast right
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //not moving

               //R/F Errors
               if (ucDataTunerB[6] & 0x10) FutabaWrite(FUTABA_WRITE | CHAR_FINV);
               else FutabaWrite(FUTABA_WRITE | CHAR_SPACE);
            }

            //Zerowanie/Strojenie
            if (!(ucButtonsTunerB & BUTTON_ZS)) FutabaWrite(FUTABA_WRITE | CHAR_ZERO); //Zerowanie
            else if (ucButtonsTunerB & BUTTON_ZS) FutabaWrite(FUTABA_WRITE | CHAR_S); //Strojenie

            //Plus (+)/Zero (0)/Minus (-)
            if (!(ucButtonsTunerB & (BUTTON_PM_1 | BUTTON_PM_2))) FutabaWrite(FUTABA_WRITE | CHAR_SPACE); //Zero (0)
            else if (ucButtonsTunerB & BUTTON_PM_1) FutabaWrite(FUTABA_WRITE | CHAR_MINUS); //Minus (-)
            else if (ucButtonsTunerB & BUTTON_PM_2) FutabaWrite(FUTABA_WRITE | CHAR_PLUS); //Plus (+)

            //Reset cursor to DDRAM start position (0x00)
            FutabaWrite(FUTABA_HOME);

            //Switch ON display (first iteration)
            if (!bDisplayON)
            {
               FutabaWrite(FUTABA_DISPLAY);
               bDisplayON = true;
            }
        }

        #endif
    }
}

void IntHandlerRS485(void)
{
    uint8_t ucByte;
    uint32_t ui32status;
    ui32status = UARTIntStatus(RS485_UART_MODULE, true);
    UARTIntClear(RS485_UART_MODULE, ui32status);

    //Receive interrupt
   if ((ui32status & UART_INT_RX) != 0u)
   {
       //Always fetch new byte
       ucByte = (CHAR) UARTCharGet(RS485_UART_MODULE);

       //Store new byte and update counter
       if (!bTunerARx && !bTunerBRx)
       {
           //New response incoming - reset counter
           if (ucByte == 'a' || ucByte == 'b')
           {
               ucByteNumUART = 0;
           }

           //Store data and update counter
           ucBufferUART[ucByteNumUART] = ucByte;
           ucByteNumUART++;

           if (ucByteNumUART == 2 && ucBufferUART[0] == 'a' && bNewButtonsA)
           {
               bNewButtonsA = true;
           }

           //Check buffer
           if (ucBufferUART[0] == 'a' && ucBufferUART[1] == (ucButtonsTunerA | BUTTON_MSG) && bNewButtonsA) //Tuner A button confirmation
           {
               //Stop timer and clear transmit flag
               StopTimerRS485();
               bTunerATx = false;
               bNewButtonsA = false;

               //Connection re-established
               if (bTimeoutTunerA)
               {
                   bTimeoutTunerA = false;
                   bReconnectedTunerA = true;
               }
           }
           else if (ucBufferUART[0] == 'b' && ucBufferUART[1] == (ucButtonsTunerB | BUTTON_MSG) && bNewButtonsB) //Tuner B button confirmation
           {
               //Stop timer and clear transmit flag
               StopTimerRS485();
               bTunerBTx = false;
               bNewButtonsB = false;

               //Connection re-established
               if (bTimeoutTunerB)
               {
                   bTimeoutTunerB = false;
                   bReconnectedTunerB = true;
               }
           }
           else if (ucBufferUART[0] == 'a' && ucByteNumUART == MAX_BYTES_UART_RX && bTunerATx) //Tuner A data received
           {
               //Stop timer, enable receive flag, clear transmit flag
               StopTimerRS485();
               bTunerARx = true;
               bTunerATx = false;

               //Connection re-established
               if (bTimeoutTunerA)
               {
                   bTimeoutTunerA = false;
                   bReconnectedTunerA = true;
               }
           }
           else if (ucBufferUART[0] == 'b' && ucByteNumUART == MAX_BYTES_UART_RX && bTunerBTx) //Tuner B data received
           {
               //Stop timer, enable receive flag, clear transmit flag
               StopTimerRS485();
               bTunerBRx = true;
               bTunerBTx = false;

               //Connection re-established
               if (bTimeoutTunerB)
               {
                   bTimeoutTunerB = false;
                   bReconnectedTunerB = true;
               }
           }

           //Reset byte counter to prevent overflow
           if (ucByteNumUART == MAX_BYTES_UART_RX) ucByteNumUART = 0;
       }
   }
}

void FutabaWrite(uint16_t msg)
{
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, 0);
    SSIDataPut(SSI3_BASE, msg);
    while (SSIBusy(SSI3_BASE)) {};
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, GPIO_PIN_3);
}

void StartTimerRS485(void)
{
    bTimeoutRS485 = false;
    TimerLoadSet(TIMER2_BASE, TIMER_BOTH, TIMEOUT_RS485);
    TimerEnable(TIMER2_BASE, TIMER_BOTH);
}

void StopTimerRS485(void)
{
    TimerDisable(TIMER2_BASE, TIMER_BOTH);
}

void StartTimerFutaba(void)
{
    TimerLoadSet(TIMER3_BASE, TIMER_BOTH, TIMEOUT_FUTABA);
    TimerEnable(TIMER3_BASE, TIMER_BOTH);
}

void StopTimerFutaba(void)
{
    TimerDisable(TIMER3_BASE, TIMER_BOTH);
}

void IntHandlerTimerRS485(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER2_BASE, TRUE);
    TimerIntClear(TIMER2_BASE, ui32status);
    bTimeoutRS485 = true;
}

void IntHandlerTimerFutaba(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER3_BASE, TRUE);
    TimerIntClear(TIMER3_BASE, ui32status);

    //Toggle display blinking flag
    if (bFillDisplay) bFillDisplay = false;
    else bFillDisplay = true;
}

void IntHandlerTimerRequest(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER4_BASE, TRUE);
    TimerIntClear(TIMER4_BASE, ui32status);

    //Switch tuner
    if (eRxMode == A) eRxMode = B;
    else eRxMode = A;

    //Set flag
    bReadNextTuner = true;
}
